# Python Script to Create Jira Ticket using GitHub Events 



## Getting started

We will learn how to automate the creation of Jira tickets directly from GitHub comments using a Python script. This solution aims to streamline the process of issue tracking and management by eliminating the need for developers to manually log in to Jira and create tickets. By leveraging GitHub webhooks and the Jira REST API, we can automate ticket creation based on specific comments made on GitHub issues, thereby enhancing productivity and ensuring that important issues are tracked systematically.

## Prerequisites

- AWS Account with Ubuntu 24.04 LTS EC2 Instance.
- A Jira account with a project set.
- Basic knowledge of python.



## Step 1:Create a Jira Project

Log in into Jira account.

After logging in, create a new project by selecting the Create project from Projects.

select the Scrum template from Software development for simplicity.

Click on Use template.

Choose a project type from team-managed project and company-managed project.

Add the project details. Provide a project name and key is generated. Click on Next.

## Step 2:Generate API Token

Go to your Jira profile.

Click on Manage your account.

Under Security, select Create and manage API token.

Click on Create API token.

Provide a label and click on Create.

Copy the generated API token cause we will be using it later.

Now our API token is also created.



