from flask import Flask, request
import requests
from requests.auth import HTTPBasicAuth
import json

app = Flask(__name__)

@app.route("/createjira", methods=['POST'])
def createjira():
    # Jira API endpoint
    url = "your_domain.atlassian.net/rest/api/3/issue"

    # Authentication with Jira API
    auth = HTTPBasicAuth("yourmail@gmail.com", "your api token")

    # HTTP headers for the request
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }

    # Example payload for Jira issue creation
    payload = json.dumps({
        "fields": {
            "description": {
                "content": [
                    {
                        "content": [
                            {
                                "text": "My first Jira ticket of the day",
                                "type": "text"
                            }
                        ],
                        "type": "paragraph"
                    }
                ],
                "type": "doc",
                "version": 1
            },
            "issuetype": {
                "id": "your_issue_id"
            },
            "project": {
                "key": "your_key"
            },
            "summary": "Main order flow broken",
        },
        "update": {}
    })

    # Get the webhook JSON data from the request
    webhook = request.json

    # Create a Jira issue if the comment contains '/jira'
    if webhook and 'comment' in webhook and webhook['comment'].get('body') == "/jira":
        response = requests.post(url, data=payload, headers=headers, auth=auth)
        return json.dumps(response.json(), sort_keys=True, indent=4, separators=(",", ": "))
    else:
        return json.dumps({"message": "Jira issue will be created if comment includes /jira"}, sort_keys=True, indent=4, separators=(",", ": "))

# Start the Flask app
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)